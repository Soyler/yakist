﻿using OpenQA.Selenium;

namespace Lr4.PageObjects
{
    public class SpecialtiesPageObject : BasePageObject
    {
        public SpecialtiesPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By SpecialtiesTab = By.CssSelector("li:nth-child(5) span:nth-child(2)");
        private By SpecialtiesHeader = By.CssSelector("h2");
        private By SpecialtiesAddButton = By.CssSelector(".addSpecialty");
        private By SpecialtiesName = By.Id("name");
        private By SpecialtiesSaveButton = By.CssSelector(".btn:nth-child(3)");
        private By SpecialtiesLastItem = By.CssSelector("tr:last-child > td > input");
        private By SpecialtiesEditButton = By.CssSelector("tr:last-child .editSpecialty");
        private By SpecialtiesUpdateButton = By.CssSelector(".updateSpecialty");
        private By SpecialtiesDeleteButton = By.CssSelector("tr:last-child .deleteSpecialty");

        public void OpenSpecialtiesPage()
        {
            driver.FindElement(SpecialtiesTab).Click();
        }

        public string GetSpecialtiesPageHeader()
        {
            return driver.FindElement(SpecialtiesHeader).Text;
        }

        public void AddSpecialty(string name)
        {
            driver.FindElement(SpecialtiesAddButton).Click();
            driver.FindElement(SpecialtiesName).Click();
            driver.FindElement(SpecialtiesName).SendKeys(name);
            driver.FindElement(SpecialtiesSaveButton).Click();
        }

        public string GetLastName()
        {
            return driver.FindElement(SpecialtiesLastItem).GetAttribute("value");
        }

        public void EditSpecialty(string name)
        {
            driver.FindElement(SpecialtiesEditButton).Click();
            driver.FindElement(SpecialtiesName).Click();
            driver.FindElement(SpecialtiesName).Clear();
            driver.FindElement(SpecialtiesName).SendKeys(name);
            driver.FindElement(SpecialtiesUpdateButton).Click();
        }

        public bool DeleteSpecialty()
        {
            string lastName = GetLastName();
            driver.FindElement(SpecialtiesDeleteButton).Click();
            Thread.Sleep(2000);
            return lastName == GetLastName();
        }
    }
}
