﻿using OpenQA.Selenium;

namespace Lr4.PageObjects
{
    public class OwnersPageObject : BasePageObject
    {
        public OwnersPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By OwnerTab = By.CssSelector(".ownerTab");
        private By OwnerTabAdd = By.CssSelector(".open li:nth-child(2) > a");
        private By OwnerTabAll = By.CssSelector(".open li:nth-child(1) > a");
        private By OwnerHeader = By.CssSelector("h2");

        public void OpenOwnersAddPage()
        {
            driver.FindElement(OwnerTab).Click();
            driver.FindElement(OwnerTabAdd).Click();
        }

        public string GetOwnersPageHeader()
        {
            return driver.FindElement(OwnerHeader).Text;
        }

        public void OpenOwnersPage()
        {
            driver.FindElement(OwnerTab).Click();
            driver.FindElement(OwnerTabAll).Click();
        }
    }
}
