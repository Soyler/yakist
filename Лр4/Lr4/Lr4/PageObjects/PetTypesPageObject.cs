﻿using OpenQA.Selenium;

namespace Lr4.PageObjects
{
    public class PetTypesPageObject : BasePageObject
    {
        public PetTypesPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By PetTypeTab = By.CssSelector("li:nth-child(4) > a");
        private By PetTypeHeader = By.CssSelector("h2");

        public void OpenPetTypesPage()
        {
            driver.FindElement(PetTypeTab).Click();
        }

        public string GetPetTypesPageHeader()
        {
            return driver.FindElement(PetTypeHeader).Text;
        }
    }
}
