﻿using OpenQA.Selenium;

namespace Lr4.PageObjects
{
    public class VeterinariansPageObject : BasePageObject
    {
        public VeterinariansPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By VetsTab = By.CssSelector(".vetsTab");
        private By VetTabAdd = By.CssSelector(".open li:nth-child(2) span:nth-child(2)");
        private By VetTabAll = By.CssSelector(".open li:nth-child(1) > a");
        private By VetHeader = By.CssSelector("h2");

        public void OpenVetsAddPage()
        {
            driver.FindElement(VetsTab).Click();
            driver.FindElement(VetTabAdd).Click();
        }

        public string GetVetsPageHeader()
        {
            return driver.FindElement(VetHeader).Text;
        }

        public void OpenVetsPage()
        {
            driver.FindElement(VetsTab).Click();
            driver.FindElement(VetTabAll).Click();
        }
    }
}
