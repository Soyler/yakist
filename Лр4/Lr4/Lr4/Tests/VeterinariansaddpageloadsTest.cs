using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class VeterinariansaddpageloadsTest : BaseTest
    {
        [Test]
        public void veterinariansaddpageloads()
        {
            VeterinariansPageObject vetsPage = new VeterinariansPageObject(driver);
            vetsPage.OpenVetsAddPage();
            Assert.That(vetsPage.GetVetsPageHeader(), Is.EqualTo("New Veterinarian"));
        }
    }
}