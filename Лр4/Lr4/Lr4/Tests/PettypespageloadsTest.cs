using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class PettypespageloadsTest : BaseTest
    {
        [Test]
        public void pettypespageloads()
        {
            PetTypesPageObject petTypesPage = new PetTypesPageObject(driver);
            petTypesPage.OpenPetTypesPage();
            Assert.That(petTypesPage.GetPetTypesPageHeader(), Is.EqualTo("Pet Types"));
        }
    }
}