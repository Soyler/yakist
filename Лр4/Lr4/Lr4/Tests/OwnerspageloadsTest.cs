using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class OwnerspageloadsTest : BaseTest
    {
        [Test]
        public void ownerspageloads()
        {
            OwnersPageObject ownersPage = new OwnersPageObject(driver);
            ownersPage.OpenOwnersPage();
            Assert.That(ownersPage.GetOwnersPageHeader(), Is.EqualTo("Owners"));
        }
    }
}