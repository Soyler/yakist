using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class HomepageloadsTest : BaseTest
    {
        [Test]
        public void homepageloads()
        {
            HomePageObject homePage = new HomePageObject(driver);
            homePage.OpenHomePage();
            Assert.That(homePage.GetHomePageHeader(), Is.EqualTo("Welcome to Petclinic"));
        }
    }
}