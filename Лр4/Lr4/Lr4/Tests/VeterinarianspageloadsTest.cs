using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class VeterinarianspageloadsTest : BaseTest
    {
        [Test]
        public void veterinarianspageloads()
        {
            VeterinariansPageObject vetsPage = new VeterinariansPageObject(driver);
            vetsPage.OpenVetsPage();
            Assert.That(vetsPage.GetVetsPageHeader(), Is.EqualTo("Veterinarians"));
        }
    }
}