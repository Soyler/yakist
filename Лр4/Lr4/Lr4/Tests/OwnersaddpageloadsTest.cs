using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class OwnersaddpageloadsTest : BaseTest
    {
        [Test]
        public void ownersaddpageloads()
        {
            OwnersPageObject ownersPage = new OwnersPageObject(driver);
            ownersPage.OpenOwnersAddPage();
            Assert.That(ownersPage.GetOwnersPageHeader(), Is.EqualTo("New Owner"));
        }
    }
}