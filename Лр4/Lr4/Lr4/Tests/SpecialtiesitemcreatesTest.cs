using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class SpecialtiesitemcreatesTest : BaseTest
    {
        [Test]
        public void specialtiesitemcreates()
        {
            SpecialtiesPageObject specialtiesPage = new SpecialtiesPageObject(driver);
            specialtiesPage.OpenSpecialtiesPage();
            Wait();
            Assert.That(specialtiesPage.GetSpecialtiesPageHeader(), Is.EqualTo("Specialties"));
            string name = "New specialty";
            specialtiesPage.AddSpecialty(name);
            Wait();
            Assert.That(specialtiesPage.GetLastName(), Is.EqualTo(name));
        }
    }
}