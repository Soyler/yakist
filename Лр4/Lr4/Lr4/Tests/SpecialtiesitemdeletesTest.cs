using NUnit.Framework;
using Lr4.PageObjects;

namespace Lr4
{
    [TestFixture]
    public class SpecialtiesitemdeletesTest : BaseTest
    {
        [Test]
        public void specialtiesitemdeletes()
        {
            SpecialtiesPageObject specialtiesPage = new SpecialtiesPageObject(driver);
            specialtiesPage.OpenSpecialtiesPage();
            Wait();
            Assert.That(specialtiesPage.GetSpecialtiesPageHeader(), Is.EqualTo("Specialties"));
            bool result = specialtiesPage.DeleteSpecialty();
            Wait();
            Assert.IsFalse(result);
        }
    }
}