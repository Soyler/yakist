﻿using Lr4.PageObjects;

namespace Lr4.PageFactory
{
    public static class Pages
    {
        public static HomePageObject Home => new HomePageObject(BaseTest.driver);
        public static OwnersPageObject Owners => new OwnersPageObject(BaseTest.driver);
        public static PetTypesPageObject PetTypes => new PetTypesPageObject(BaseTest.driver);
        public static SpecialtiesPageObject Specs => new SpecialtiesPageObject(BaseTest.driver);
        public static VeterinariansPageObject Vets => new VeterinariansPageObject(BaseTest.driver);
    }
}
