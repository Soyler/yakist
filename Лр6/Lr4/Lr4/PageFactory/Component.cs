﻿using Lr4.PageComponents;

namespace Lr4.PageFactory
{
    public static class Component
    {
        public static TabsComponent Tabs => new TabsComponent(BaseTest.driver);
    }
}
