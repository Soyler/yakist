﻿using NUnit.Allure.Attributes;
using OpenQA.Selenium;

namespace Lr4.PageObjects
{
    public class SpecialtiesPageObject : BasePageObject
    {
        public SpecialtiesPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By SpecialtiesHeader = By.CssSelector("h2");
        private By SpecialtiesAddButton = By.CssSelector(".addSpecialty");
        private By SpecialtiesName = By.Id("name");
        private By SpecialtiesSaveButton = By.CssSelector(".btn:nth-child(3)");
        private By SpecialtiesLastItem = By.CssSelector("tr:last-child > td > input");
        private By SpecialtiesEditButton = By.CssSelector("tr:last-child .editSpecialty");
        private By SpecialtiesUpdateButton = By.CssSelector(".updateSpecialty");
        private By SpecialtiesDeleteButton = By.CssSelector("tr:last-child .deleteSpecialty");

        [AllureStep("Get specialties page header text")]
        public string GetSpecialtiesPageHeader()
        {
            return driver.FindElement(SpecialtiesHeader).Text;
        }

        [AllureStep("Add specialty with specified name")]
        public void AddSpecialty(string name)
        {
            driver.FindElement(SpecialtiesAddButton).Click();
            Helper.ClickAndSendKeys(driver.FindElement(SpecialtiesName), name);
            driver.FindElement(SpecialtiesSaveButton).Click();
        }

        [AllureStep("Get name of last specialties item")]
        public string GetLastName()
        {
            return driver.FindElement(SpecialtiesLastItem).GetAttribute("value");
        }

        [AllureStep("Edit specialties with specified name")]
        public void EditSpecialty(string name)
        {
            driver.FindElement(SpecialtiesEditButton).Click();
            Helper.ClickAndClearAndSendKeys(driver.FindElement(SpecialtiesName), name);
            driver.FindElement(SpecialtiesUpdateButton).Click();
        }

        [AllureStep("Delete last specialty. Compare previous and current last item names")]
        public bool DeleteSpecialty()
        {
            string lastName = GetLastName();
            driver.FindElement(SpecialtiesDeleteButton).Click();
            Helper.Wait();
            return lastName == GetLastName();
        }
    }
}
