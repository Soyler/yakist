using NUnit.Framework;
using Lr4.PageObjects;
using Lr4.PageComponents;
using Lr4.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr4
{
    [TestFixture]
    [AllureNUnit]
    public class SpecialtiesitemdeletesTest : BaseTest
    {
        [Test, Description("This test checks that specialty could be deleted successfully")]
        [AllureSuite("Specialties")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void specialtiesitemdeletes()
        {
            TabsComponent tabs = Component.Tabs;
            tabs.OpenSpecialtiesPage();
            Helper.Wait();
            SpecialtiesPageObject specialtiesPage = Pages.Specs;
            Assert.That(specialtiesPage.GetSpecialtiesPageHeader(), Is.EqualTo("Specialties"));
            bool result = specialtiesPage.DeleteSpecialty();
            Helper.Wait();
            Assert.IsFalse(result);
        }
    }
}