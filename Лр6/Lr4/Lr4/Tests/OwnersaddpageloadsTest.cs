using NUnit.Framework;
using Lr4.PageObjects;
using Lr4.PageComponents;
using Lr4.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr4
{
    [TestFixture]
    [AllureNUnit]
    public class OwnersaddpageloadsTest : BaseTest
    {
        [Test, Description("This test checks that owner add page could be loaded successfully")]
        [AllureSuite("Owners")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void ownersaddpageloads()
        {
            TabsComponent tabs = Component.Tabs;
            tabs.OpenOwnersAddPage();
            OwnersPageObject ownersPage = Pages.Owners;
            Assert.That(ownersPage.GetOwnersPageHeader(), Is.EqualTo("New Owner"));
        }
    }
}