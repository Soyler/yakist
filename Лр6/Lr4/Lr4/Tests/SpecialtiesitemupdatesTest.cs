using NUnit.Framework;
using Lr4.PageObjects;
using Lr4.PageComponents;
using Lr4.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr4
{
    [TestFixture]
    [AllureNUnit]
    public class SpecialtiesitemupdatesTest : BaseTest
    {
        [Test, Description("This test checks that specialty could be updated successfully")]
        [AllureSuite("Specialties")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void specialtiesitemupdates()
        {
            TabsComponent tabs = Component.Tabs;
            tabs.OpenSpecialtiesPage();
            Helper.Wait();
            SpecialtiesPageObject specialtiesPage = Pages.Specs;
            Assert.That(specialtiesPage.GetSpecialtiesPageHeader(), Is.EqualTo("Specialties"));
            string name = "New specialty 2";
            specialtiesPage.EditSpecialty(name);
            Helper.Wait();
            Assert.That(specialtiesPage.GetLastName(), Is.EqualTo(name));
        }
    }
}