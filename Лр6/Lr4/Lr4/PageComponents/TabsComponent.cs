﻿using NUnit.Allure.Attributes;
using OpenQA.Selenium;

namespace Lr4.PageComponents
{
    public class TabsComponent
    {
        public TabsComponent(IWebDriver driver)
        {
            this.driver = driver;
        }

        private IWebDriver driver;
        private By HomeTab = By.CssSelector(".glyphicon-home");
        private By OwnerTab = By.CssSelector(".ownerTab");
        private By OwnerTabAdd = By.CssSelector(".open li:nth-child(2) > a");
        private By OwnerTabAll = By.CssSelector(".open li:nth-child(1) > a");
        private By PetTypeTab = By.CssSelector("li:nth-child(4) > a");
        private By SpecialtiesTab = By.CssSelector("li:nth-child(5) span:nth-child(2)");
        private By VetsTab = By.CssSelector(".vetsTab");
        private By VetTabAdd = By.CssSelector(".open li:nth-child(2) span:nth-child(2)");
        private By VetTabAll = By.CssSelector(".open li:nth-child(1) > a");

        [AllureStep("Open home page")]
        public void OpenHomePage()
        {
            driver.FindElement(HomeTab).Click();
        }

        [AllureStep("Open owners add page")]
        public void OpenOwnersAddPage()
        {
            driver.FindElement(OwnerTab).Click();
            driver.FindElement(OwnerTabAdd).Click();
        }

        [AllureStep("Open owners page")]
        public void OpenOwnersPage()
        {
            driver.FindElement(OwnerTab).Click();
            driver.FindElement(OwnerTabAll).Click();
        }

        [AllureStep("Open pet types page")]
        public void OpenPetTypesPage()
        {
            driver.FindElement(PetTypeTab).Click();
        }

        [AllureStep("Open specialties page")]
        public void OpenSpecialtiesPage()
        {
            driver.FindElement(SpecialtiesTab).Click();
        }

        [AllureStep("Open vets add page")]
        public void OpenVetsAddPage()
        {
            driver.FindElement(VetsTab).Click();
            driver.FindElement(VetTabAdd).Click();
        }

        [AllureStep("Open vets page")]
        public void OpenVetsPage()
        {
            driver.FindElement(VetsTab).Click();
            driver.FindElement(VetTabAll).Click();
        }
    }
}
