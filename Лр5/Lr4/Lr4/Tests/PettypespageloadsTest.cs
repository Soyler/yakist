using NUnit.Framework;
using Lr4.PageObjects;
using Lr4.PageComponents;
using Lr4.PageFactory;

namespace Lr4
{
    [TestFixture]
    public class PettypespageloadsTest : BaseTest
    {
        [Test]
        public void pettypespageloads()
        {
            TabsComponent tabs = Component.Tabs;
            tabs.OpenPetTypesPage();
            PetTypesPageObject petTypesPage = Pages.PetTypes;
            Assert.That(petTypesPage.GetPetTypesPageHeader(), Is.EqualTo("Pet Types"));
        }
    }
}