using NUnit.Framework;
using Lr4.PageObjects;
using Lr4.PageComponents;
using Lr4.PageFactory;

namespace Lr4
{
    [TestFixture]
    public class VeterinariansaddpageloadsTest : BaseTest
    {
        [Test]
        public void veterinariansaddpageloads()
        {
            TabsComponent tabs = Component.Tabs;
            tabs.OpenVetsAddPage();
            VeterinariansPageObject vetsPage = Pages.Vets;
            Assert.That(vetsPage.GetVetsPageHeader(), Is.EqualTo("New Veterinarian"));
        }
    }
}