using NUnit.Framework;
using Lr4.PageObjects;
using Lr4.PageComponents;
using Lr4.PageFactory;

namespace Lr4
{
    [TestFixture]
    public class OwnerspageloadsTest : BaseTest
    {
        [Test]
        public void ownerspageloads()
        {
            TabsComponent tabs = Component.Tabs;
            tabs.OpenOwnersPage();
            OwnersPageObject ownersPage = Pages.Owners;
            Assert.That(ownersPage.GetOwnersPageHeader(), Is.EqualTo("Owners"));
        }
    }
}