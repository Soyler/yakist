using NUnit.Framework;
using Lr4.PageObjects;
using Lr4.PageComponents;
using Lr4.PageFactory;

namespace Lr4
{
    [TestFixture]
    public class OwnersaddpageloadsTest : BaseTest
    {
        [Test]
        public void ownersaddpageloads()
        {
            TabsComponent tabs = Component.Tabs;
            tabs.OpenOwnersAddPage();
            OwnersPageObject ownersPage = Pages.Owners;
            Assert.That(ownersPage.GetOwnersPageHeader(), Is.EqualTo("New Owner"));
        }
    }
}