using NUnit.Framework;
using Lr4.PageObjects;
using Lr4.PageComponents;
using Lr4.PageFactory;

namespace Lr4
{
    [TestFixture]
    public class SpecialtiesitemcreatesTest : BaseTest
    {
        [Test]
        public void specialtiesitemcreates()
        {
            TabsComponent tabs = Component.Tabs;
            tabs.OpenSpecialtiesPage();
            Helper.Wait();
            SpecialtiesPageObject specialtiesPage = Pages.Specs;
            Assert.That(specialtiesPage.GetSpecialtiesPageHeader(), Is.EqualTo("Specialties"));
            string name = "New specialty";
            specialtiesPage.AddSpecialty(name);
            Helper.Wait();
            Assert.That(specialtiesPage.GetLastName(), Is.EqualTo(name));
        }
    }
}