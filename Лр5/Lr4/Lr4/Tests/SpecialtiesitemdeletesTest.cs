using NUnit.Framework;
using Lr4.PageObjects;
using Lr4.PageComponents;
using Lr4.PageFactory;

namespace Lr4
{
    [TestFixture]
    public class SpecialtiesitemdeletesTest : BaseTest
    {
        [Test]
        public void specialtiesitemdeletes()
        {
            TabsComponent tabs = Component.Tabs;
            tabs.OpenSpecialtiesPage();
            Helper.Wait();
            SpecialtiesPageObject specialtiesPage = Pages.Specs;
            Assert.That(specialtiesPage.GetSpecialtiesPageHeader(), Is.EqualTo("Specialties"));
            bool result = specialtiesPage.DeleteSpecialty();
            Helper.Wait();
            Assert.IsFalse(result);
        }
    }
}