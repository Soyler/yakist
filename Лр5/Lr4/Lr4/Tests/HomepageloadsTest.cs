using NUnit.Framework;
using Lr4.PageObjects;
using Lr4.PageComponents;
using Lr4.PageFactory;

namespace Lr4
{
    [TestFixture]
    public class HomepageloadsTest : BaseTest
    {
        [Test]
        public void homepageloads()
        {
            TabsComponent tabs = Component.Tabs;
            tabs.OpenHomePage();
            HomePageObject homePage = Pages.Home;
            Assert.That(homePage.GetHomePageHeader(), Is.EqualTo("Welcome to Petclinic"));
        }
    }
}