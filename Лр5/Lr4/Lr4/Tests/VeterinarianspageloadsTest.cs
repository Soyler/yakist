using NUnit.Framework;
using Lr4.PageObjects;
using Lr4.PageComponents;
using Lr4.PageFactory;

namespace Lr4
{
    [TestFixture]
    public class VeterinarianspageloadsTest : BaseTest
    {
        [Test]
        public void veterinarianspageloads()
        {
            TabsComponent tabs = Component.Tabs;
            tabs.OpenVetsPage();
            VeterinariansPageObject vetsPage = Pages.Vets;
            Assert.That(vetsPage.GetVetsPageHeader(), Is.EqualTo("Veterinarians"));
        }
    }
}