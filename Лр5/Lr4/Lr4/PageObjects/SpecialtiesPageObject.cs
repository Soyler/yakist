﻿using OpenQA.Selenium;

namespace Lr4.PageObjects
{
    public class SpecialtiesPageObject : BasePageObject
    {
        public SpecialtiesPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By SpecialtiesHeader = By.CssSelector("h2");
        private By SpecialtiesAddButton = By.CssSelector(".addSpecialty");
        private By SpecialtiesName = By.Id("name");
        private By SpecialtiesSaveButton = By.CssSelector(".btn:nth-child(3)");
        private By SpecialtiesLastItem = By.CssSelector("tr:last-child > td > input");
        private By SpecialtiesEditButton = By.CssSelector("tr:last-child .editSpecialty");
        private By SpecialtiesUpdateButton = By.CssSelector(".updateSpecialty");
        private By SpecialtiesDeleteButton = By.CssSelector("tr:last-child .deleteSpecialty");

        public string GetSpecialtiesPageHeader()
        {
            return driver.FindElement(SpecialtiesHeader).Text;
        }

        public void AddSpecialty(string name)
        {
            driver.FindElement(SpecialtiesAddButton).Click();
            Helper.ClickAndSendKeys(driver.FindElement(SpecialtiesName), name);
            driver.FindElement(SpecialtiesSaveButton).Click();
        }

        public string GetLastName()
        {
            return driver.FindElement(SpecialtiesLastItem).GetAttribute("value");
        }

        public void EditSpecialty(string name)
        {
            driver.FindElement(SpecialtiesEditButton).Click();
            Helper.ClickAndClearAndSendKeys(driver.FindElement(SpecialtiesName), name);
            driver.FindElement(SpecialtiesUpdateButton).Click();
        }

        public bool DeleteSpecialty()
        {
            string lastName = GetLastName();
            driver.FindElement(SpecialtiesDeleteButton).Click();
            Helper.Wait();
            return lastName == GetLastName();
        }
    }
}
