﻿using OpenQA.Selenium;

namespace Lr4.PageObjects
{
    public class PetTypesPageObject : BasePageObject
    {
        public PetTypesPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By PetTypeHeader = By.CssSelector("h2");

        public string GetPetTypesPageHeader()
        {
            return driver.FindElement(PetTypeHeader).Text;
        }
    }
}
