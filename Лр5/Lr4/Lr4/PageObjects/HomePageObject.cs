﻿using OpenQA.Selenium;

namespace Lr4.PageObjects
{
    public class HomePageObject : BasePageObject
    {
        public HomePageObject(IWebDriver driver) : base(driver)
        {

        }

        private By HomeHeader = By.CssSelector("h1");

        public string GetHomePageHeader()
        {
            return driver.FindElement(HomeHeader).Text;
        }
    }
}
