﻿using OpenQA.Selenium;

namespace Lr4.PageObjects
{
    public class VeterinariansPageObject : BasePageObject
    {
        public VeterinariansPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By VetHeader = By.CssSelector("h2");

        public string GetVetsPageHeader()
        {
            return driver.FindElement(VetHeader).Text;
        }
    }
}
