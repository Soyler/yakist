﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lr4.PageComponents
{
    public class TabsComponent
    {
        public TabsComponent(IWebDriver driver)
        {
            this.driver = driver;
        }

        private IWebDriver driver;
        private By HomeTab = By.CssSelector(".glyphicon-home");
        private By OwnerTab = By.CssSelector(".ownerTab");
        private By OwnerTabAdd = By.CssSelector(".open li:nth-child(2) > a");
        private By OwnerTabAll = By.CssSelector(".open li:nth-child(1) > a");
        private By PetTypeTab = By.CssSelector("li:nth-child(4) > a");
        private By SpecialtiesTab = By.CssSelector("li:nth-child(5) span:nth-child(2)");
        private By VetsTab = By.CssSelector(".vetsTab");
        private By VetTabAdd = By.CssSelector(".open li:nth-child(2) span:nth-child(2)");
        private By VetTabAll = By.CssSelector(".open li:nth-child(1) > a");

        public void OpenHomePage()
        {
            driver.FindElement(HomeTab).Click();
        }

        public void OpenOwnersAddPage()
        {
            driver.FindElement(OwnerTab).Click();
            driver.FindElement(OwnerTabAdd).Click();
        }

        public void OpenOwnersPage()
        {
            driver.FindElement(OwnerTab).Click();
            driver.FindElement(OwnerTabAll).Click();
        }

        public void OpenPetTypesPage()
        {
            driver.FindElement(PetTypeTab).Click();
        }

        public void OpenSpecialtiesPage()
        {
            driver.FindElement(SpecialtiesTab).Click();
        }

        public void OpenVetsAddPage()
        {
            driver.FindElement(VetsTab).Click();
            driver.FindElement(VetTabAdd).Click();
        }

        public void OpenVetsPage()
        {
            driver.FindElement(VetsTab).Click();
            driver.FindElement(VetTabAll).Click();
        }
    }
}
