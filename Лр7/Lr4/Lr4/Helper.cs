﻿using OpenQA.Selenium;

namespace Lr4
{
    class Helper
    {
        public static void Wait()
        {
            Thread.Sleep(1500);
        }

        public static void ClickAndSendKeys(IWebElement webElement, string name)
        {
            webElement.Click();
            webElement.SendKeys(name);
        }

        public static void ClickAndClearAndSendKeys(IWebElement webElement, string name)
        {
            webElement.Click();
            webElement.Clear();
            webElement.SendKeys(name);
        }
    }
}
