﻿using NUnit.Allure.Attributes;
using OpenQA.Selenium;

namespace Lr4.PageObjects
{
    public class PetTypesPageObject : BasePageObject
    {
        public PetTypesPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By PetTypeHeader = By.CssSelector("h2");

        [AllureStep("Get pet types page header text")]
        public string GetPetTypesPageHeader()
        {
            return driver.FindElement(PetTypeHeader).Text;
        }
    }
}
