﻿using NUnit.Allure.Attributes;
using OpenQA.Selenium;

namespace Lr4.PageObjects
{
    public class VeterinariansPageObject : BasePageObject
    {
        public VeterinariansPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By VetHeader = By.CssSelector("h2");

        [AllureStep("Get vets page header text")]
        public string GetVetsPageHeader()
        {
            return driver.FindElement(VetHeader).Text;
        }
    }
}
