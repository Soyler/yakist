﻿using NUnit.Allure.Attributes;
using OpenQA.Selenium;

namespace Lr4.PageObjects
{
    public class OwnersPageObject : BasePageObject
    {
        public OwnersPageObject(IWebDriver driver) : base(driver)
        {

        }

        private By OwnerHeader = By.CssSelector("h2");

        [AllureStep("Get owners page header text")]
        public string GetOwnersPageHeader()
        {
            return driver.FindElement(OwnerHeader).Text;
        }
    }
}
