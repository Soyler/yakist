using NUnit.Framework;
using Lr4.PageObjects;
using Lr4.PageComponents;
using Lr4.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr4
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class SpecialtiesitemcreatesTest : BaseTest
    {
        [TestCase("New specialty 1")]
        [TestCase("New specialty 2")]
        [TestCase("New specialty 3")]
        [Test, Description("This test checks that specialty could be created successfully")]
        [AllureSuite("Specialties")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void specialtiesitemcreates(string name)
        {
            TabsComponent tabs = Component.Tabs;
            tabs.OpenSpecialtiesPage();
            Helper.Wait();
            SpecialtiesPageObject specialtiesPage = Pages.Specs;
            Assert.That(specialtiesPage.GetSpecialtiesPageHeader(), Is.EqualTo("Specialties"));
            specialtiesPage.AddSpecialty(name);
            Helper.Wait();
            Assert.That(specialtiesPage.GetLastName(), Is.EqualTo(name));
        }
    }
}