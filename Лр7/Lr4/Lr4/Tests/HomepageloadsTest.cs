using NUnit.Framework;
using Lr4.PageObjects;
using Lr4.PageComponents;
using Lr4.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr4
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class HomepageloadsTest : BaseTest
    {
        [Test, Description("This test checks that home page could be loaded successfully")]
        [AllureSuite("Home")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void homepageloads()
        {
            TabsComponent tabs = Component.Tabs;
            tabs.OpenHomePage();
            HomePageObject homePage = Pages.Home;
            Assert.That(homePage.GetHomePageHeader(), Is.EqualTo("Welcome to Petclinic"));
        }
    }
}