using NUnit.Framework;
using Lr4.PageObjects;
using Lr4.PageComponents;
using Lr4.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr4
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class SpecialtiespageloadsTest : BaseTest
    {
        [Test, Description("This test checks that specialties page could be loaded successfully")]
        [AllureSuite("Specialties")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void specialtiespageloads([Values("Specialties", "Specialty", "Specs")] string pageName)
        {
            TabsComponent tabs = Component.Tabs;
            tabs.OpenSpecialtiesPage();
            SpecialtiesPageObject specialtiesPage = Pages.Specs;
            Assert.That(specialtiesPage.GetSpecialtiesPageHeader(), Is.EqualTo(pageName));
        }
    }
}