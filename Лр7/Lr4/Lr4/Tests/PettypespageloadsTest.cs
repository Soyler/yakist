using NUnit.Framework;
using Lr4.PageObjects;
using Lr4.PageComponents;
using Lr4.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr4
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class PettypespageloadsTest : BaseTest
    {
        [Test, Description("This test checks that pet types page could be loaded successfully")]
        [AllureSuite("Pet types")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void pettypespageloads()
        {
            TabsComponent tabs = Component.Tabs;
            tabs.OpenPetTypesPage();
            PetTypesPageObject petTypesPage = Pages.PetTypes;
            Assert.That(petTypesPage.GetPetTypesPageHeader(), Is.EqualTo("Pet Types"));
        }
    }
}