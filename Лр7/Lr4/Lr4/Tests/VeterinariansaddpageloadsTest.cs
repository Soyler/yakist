using NUnit.Framework;
using Lr4.PageObjects;
using Lr4.PageComponents;
using Lr4.PageFactory;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;

namespace Lr4
{
    [TestFixture]
    [AllureNUnit]
    [Parallelizable]
    public class VeterinariansaddpageloadsTest : BaseTest
    {
        [Test, Description("This test checks that veterinarians add page could be loaded successfully")]
        [AllureSuite("Veterinarians")]
        [AllureStory]
        [AllureTag("NUnit")]
        public void veterinariansaddpageloads()
        {
            TabsComponent tabs = Component.Tabs;
            tabs.OpenVetsAddPage();
            VeterinariansPageObject vetsPage = Pages.Vets;
            Assert.That(vetsPage.GetVetsPageHeader(), Is.EqualTo("New Veterinarian"));
        }
    }
}